import csv


class ProcessFile:
    def __init__(self, file, delimiter):
        self.__file = file
        self.__delimiter = delimiter

    def process_file_to_convert(self):
        try:
            f = open(self.__file, "r")
            data = []
            csv_file = csv.reader(f, delimiter=self.__delimiter)
            for i, row in enumerate(csv_file):
                print(i, row)
        except AttributeError:
            raise AttributeError("Any attribute don't exists in this context")
        except Exception:
            raise Exception('It was not possible to process the file. Try again')
