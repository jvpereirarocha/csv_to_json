import setuptools

with open("README.md", "r") as file:
    long_description = file.read()

setuptools.setup(
    name='csv-to-json-jvpr',
    version='0.0.1',
    author='Joao Victor Rocha',
    author_email='jvpereirarocha@gmail.com',
    description='A package to convert CSV files to JSON',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language": "Python 3",
        "License :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6'
)
